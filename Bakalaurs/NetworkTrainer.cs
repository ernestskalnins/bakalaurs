﻿using Bakalaurs.Data.Db;
using Bakalaurs.Data.Models;
using CNNImplementation.CNN;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalaurs
{
    public class NetworkTrainer
    {
        private List<InputImage> _inputImage;

        public NetworkTrainer()
        {
            _inputImage = new List<InputImage>();
        }

        public CNNImplementation.CNN.Network Train(CNNImplementation.CNN.Network network, int iterations)
        {
            GetDataset();
            int batchSize = _inputImage.Count / 10;
            int imagesUsed = 0;
            while (imagesUsed < batchSize * 10)
            {
                List<InputImage> imageBatch = _inputImage.GetRange(imagesUsed, batchSize);
                network.TrainBatch(imageBatch, iterations);
                imagesUsed += batchSize;
            }
            return network;
        }

        private void GetDataset()
        {
            using (var db = new ImageDbContext())
            {
                var images = (from o in db.Observations
                orderby o.UtcTimestamp descending
                select o.Image.Data).ToList();

                var commandValues = (from o in db.Observations
                orderby o.UtcTimestamp descending
                select o.ControValue).ToList();
                
                for (int i = 0; i < images.Count; i++)
                {
                    _inputImage.Add(
                        ConvertToInputImage(images[i], commandValues[i])    
                    ); 
                }
            }
        }

        private InputImage ConvertToInputImage(byte[] imageBytes, ControlValue controlValue)
        {
            Bitmap bmp = this.BytesToBitmap(imageBytes);
            InputImage inputImage = new InputImage(bmp);
            inputImage.value = new List<double>()
            {
                controlValue.Left,
                controlValue.Forward,
                controlValue.Right
            };
            return inputImage;
        }

        private Bitmap BytesToBitmap(byte[] imageBytes)
        {
            Bitmap bmp;
            using (var ms = new MemoryStream(imageBytes))
            {
                bmp = new Bitmap(ms);
            }

            return bmp;
        }
    }
}
