﻿using Bakalaurs.Data.Db;
using CNNImplementation.CNN;
using CNNImplementation.CNN.CNNLayers;
using CNNImplementation.CNN.NN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Bakalaurs
{
    class CNNWrapper
    {
        public Network Network { get; set; }

        private double[,] filter1 = new double[,]
        {   
                { 1, 0, -1 },
                { 1, 0, -1 },
                { 1, 0, -1 }
        };

        private double[,] filter2 = new double[,]
        {
                { 1, 1, 1 },
                { 0, 0, 0 },
                { -1, -1, -1 }
        };

        public CNNWrapper(double trainingRate)
        {
            List<Filter> filters = new List<Filter>();
            filters.Add(new Filter(filter1, 1, 1));
            filters.Add(new Filter(filter2, 1, 1));

            Network = new Network(trainingRate);
            Network.AddConvolutionLayer(filters)
                   .AddRectifierLayer(new Rectifier())
                   .AddPoolingLayer(new Pool(16, 16, 16))
                   .AddFullyConnectedLayer(160, 100, 3);
        }

        public void Store()
        {
            using (var db = new ImageDbContext())
            {
                var network = new Data.Models.Network() { Layers = new List<Data.Models.Layer>() };

                foreach (var layer in Network.NeuralNetwork.Layers)
                {
                    var nnLayer = new Data.Models.Layer()
                    {
                        Neurons = new List<Data.Models.Neuron>(),
                        Index = Network.NeuralNetwork.Layers.IndexOf(layer)
                    };
                    foreach (var neuron in layer.Neurons)
                    {
                        var nnNeuron = new Data.Models.Neuron()
                        {
                            Bias = neuron.Bias,
                            Index = layer.Neurons.IndexOf(neuron),
                            Dendrites = new List<Data.Models.Dendrite>()
                        };
                        foreach (var dendrite in neuron.Dendrites)
                        {
                            var nnDendrite = new Data.Models.Dendrite()
                            {
                                Index = neuron.Dendrites.IndexOf(dendrite),
                                Value = dendrite.Weight
                            };
                            nnNeuron.Dendrites.Add(nnDendrite);
                        }
                        nnLayer.Neurons.Add(nnNeuron);
                    }
                    network.Layers.Add(nnLayer);
                }

                db.Networks.Add(network);

                db.SaveChanges();
            }
        }

        public void Load(Guid? networkId = null)
        {
            using (var db = new ImageDbContext())
            {
                var network = (from n in db.Networks
                               where networkId.HasValue || 1 == 1
                               orderby n.CreatedUtcTimestamp descending
                               select n).FirstOrDefault();

                if(network == null)
                {
                    Console.WriteLine("No network, no fun!");
                    return;
                }

                var nnLayers = db.Layers
                    .Include(l => l.Neurons)
                    .Where(l => l.NetworkId == network.Id).OrderBy(l => l.Index).ToList();

                var layers = new List<Layer>();

                foreach (var nnLayer in nnLayers)
                {
                    var layer = new Layer(nnLayer.Neurons.Count);

                    foreach(var nnNeuron in nnLayer.Neurons)
                    {
                        var neuron = new Neuron()
                        {
                            Bias = nnNeuron.Bias,
                        };

                        var nnDendrites = db.Dendrites.Where(d => d.NeuronId == nnNeuron.Id).OrderBy(d => d.Index).ToList();

                        neuron.Dendrites.AddRange(
                            nnDendrites.Select(d => new Dendrite()
                            {
                                Weight = d.Value
                            })
                        );
                        layer.Neurons.Add(neuron);
                    }

                    layers.Add(layer);
                    this.Network.NeuralNetwork.Layers = layers;
                }
            }

        }
    }
}
