﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using Bakalaurs.Data.Db;
using Bakalaurs.Server;
using System.Linq;
using Bakalaurs.Data;
using System.Drawing;
using CNNImplementation.CNN.CNNLayers;
using System.Collections.Generic;
using CNNImplementation.CNN;
using System.Windows.Navigation;

namespace Bakalaurs
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Content = new DataStore();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //
        }

        private void Store_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new DataStore();
        }

        private void Train_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new NNTrainer();
        }

        private void Predict_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new Predict();
        }

        /*
        private void ShowImage(ClientObservation clientObservation)
        {
            // this.SaveObservation(clientObservation);
            Dispatcher.Invoke(() =>
            {
                this.cameraImage.Source = BytesToBitmapImage(clientObservation.image);
                this.commandDrive.Text = clientObservation.drive.ToString();
                this.commandSteer.Text = clientObservation.steer.ToString();
                this.predictDrive.Text = clientObservation.predictDrive.ToString();
                this.predictSteer.Text = clientObservation.predictSteer.ToString();
            });
        }
        */

        /*
        private double[] Test(ClientObservation clientObservation)
        {
            List<double> values = new List<double> { clientObservation.drive, clientObservation.steer };
            InputImage image = new InputImage(BytesToBitmap(clientObservation.image));
            image.value = values;

            return _network.TrainSingle(image);
        }
        */
        /*
        */

        /*
        private void ShowLatestImage()
        {
            using (var db = new ImageDbContext())
            {
                var data = (from o in db.Observations
                orderby o.UtcTimestamp descending
                select o.Image.Data).FirstOrDefault();

                if (data != null)
                {
                    this.cameraImage.Source = BytesToBitmapImage(data);
                }
            }
        }
        */
    }
}