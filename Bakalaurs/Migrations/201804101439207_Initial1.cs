namespace Bakalaurs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Neurons", "Bias", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Neurons", "Bias");
        }
    }
}
