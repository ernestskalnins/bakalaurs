namespace Bakalaurs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ControlValues",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Steer = c.Double(nullable: false),
                        Drive = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dendrites",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Index = c.Int(nullable: false),
                        Value = c.Double(nullable: false),
                        NeuronId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Neurons", t => t.NeuronId, cascadeDelete: true)
                .Index(t => t.Index, name: "IX_Dendrite__Index")
                .Index(t => t.NeuronId, name: "IX_Dendrite__NeuronId");
            
            CreateTable(
                "dbo.Neurons",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Index = c.Int(nullable: false),
                        Bias = c.Double(nullable: false),
                        LayerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Layers", t => t.LayerId, cascadeDelete: true)
                .Index(t => t.Index, name: "IX_Neuron__Index")
                .Index(t => t.LayerId, name: "IX_Neuron__LayerId");
            
            CreateTable(
                "dbo.Layers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Index = c.Int(nullable: false),
                        NetworkId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Networks", t => t.NetworkId, cascadeDelete: true)
                .Index(t => t.Index, name: "IX_Layer__Index")
                .Index(t => t.NetworkId, name: "IX_Layer__NetworkId");
            
            CreateTable(
                "dbo.Networks",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CreatedUtcTimestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.CreatedUtcTimestamp, name: "IX_Network__CreatedUtcTimestamp");
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Data = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Observations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UtcTimestamp = c.DateTime(nullable: false),
                        ControlValueId = c.Guid(nullable: false),
                        ImageId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ControlValues", t => t.ControlValueId, cascadeDelete: true)
                .ForeignKey("dbo.Images", t => t.ImageId, cascadeDelete: true)
                .Index(t => t.UtcTimestamp, name: "IX_Observation__UtcTimestamp")
                .Index(t => t.ControlValueId, name: "IX_Observation__ControlValueId")
                .Index(t => t.ImageId, name: "IX_Observation__ImageId");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Observations", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Observations", "ControlValueId", "dbo.ControlValues");
            DropForeignKey("dbo.Neurons", "LayerId", "dbo.Layers");
            DropForeignKey("dbo.Layers", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.Dendrites", "NeuronId", "dbo.Neurons");
            DropIndex("dbo.Observations", "IX_Observation__ImageId");
            DropIndex("dbo.Observations", "IX_Observation__ControlValueId");
            DropIndex("dbo.Observations", "IX_Observation__UtcTimestamp");
            DropIndex("dbo.Networks", "IX_Network__CreatedUtcTimestamp");
            DropIndex("dbo.Layers", "IX_Layer__NetworkId");
            DropIndex("dbo.Layers", "IX_Layer__Index");
            DropIndex("dbo.Neurons", "IX_Neuron__LayerId");
            DropIndex("dbo.Neurons", "IX_Neuron__Index");
            DropIndex("dbo.Dendrites", "IX_Dendrite__NeuronId");
            DropIndex("dbo.Dendrites", "IX_Dendrite__Index");
            DropTable("dbo.Observations");
            DropTable("dbo.Images");
            DropTable("dbo.Networks");
            DropTable("dbo.Layers");
            DropTable("dbo.Neurons");
            DropTable("dbo.Dendrites");
            DropTable("dbo.ControlValues");
        }
    }
}
