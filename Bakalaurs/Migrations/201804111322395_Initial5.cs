namespace Bakalaurs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial5 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Dendrites");
            AlterColumn("dbo.Dendrites", "Id", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.Dendrites", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Dendrites");
            AlterColumn("dbo.Dendrites", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Dendrites", "Id");
        }
    }
}
