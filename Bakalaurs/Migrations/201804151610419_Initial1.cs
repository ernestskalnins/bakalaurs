namespace Bakalaurs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ControlValues", "Left", c => c.Double(nullable: false));
            AddColumn("dbo.ControlValues", "Forward", c => c.Double(nullable: false));
            AddColumn("dbo.ControlValues", "Right", c => c.Double(nullable: false));
            DropColumn("dbo.ControlValues", "Steer");
            DropColumn("dbo.ControlValues", "Drive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ControlValues", "Drive", c => c.Double(nullable: false));
            AddColumn("dbo.ControlValues", "Steer", c => c.Double(nullable: false));
            DropColumn("dbo.ControlValues", "Right");
            DropColumn("dbo.ControlValues", "Forward");
            DropColumn("dbo.ControlValues", "Left");
        }
    }
}
