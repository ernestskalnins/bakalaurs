namespace Bakalaurs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dendrites",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Value = c.Double(nullable: false),
                        Neuron_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Neurons", t => t.Neuron_Id)
                .Index(t => t.Neuron_Id);
            
            CreateTable(
                "dbo.Layers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Network_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Networks", t => t.Network_Id)
                .Index(t => t.Network_Id);
            
            CreateTable(
                "dbo.Neurons",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Bias = c.Double(nullable: false),
                        Layer_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Layers", t => t.Layer_Id)
                .Index(t => t.Layer_Id);
            
            CreateTable(
                "dbo.Networks",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Layers", "Network_Id", "dbo.Networks");
            DropForeignKey("dbo.Neurons", "Layer_Id", "dbo.Layers");
            DropForeignKey("dbo.Dendrites", "Neuron_Id", "dbo.Neurons");
            DropIndex("dbo.Neurons", new[] { "Layer_Id" });
            DropIndex("dbo.Layers", new[] { "Network_Id" });
            DropIndex("dbo.Dendrites", new[] { "Neuron_Id" });
            DropTable("dbo.Networks");
            DropTable("dbo.Neurons");
            DropTable("dbo.Layers");
            DropTable("dbo.Dendrites");
        }
    }
}
