namespace Bakalaurs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Layers", "Network_Id", "dbo.Networks");
            DropIndex("dbo.Dendrites", new[] { "Neuron_Id" });
            DropIndex("dbo.Neurons", new[] { "Layer_Id" });
            DropPrimaryKey("dbo.Dendrites");
            AlterColumn("dbo.Dendrites", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Dendrites", "Neuron_Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Neurons", "Layer_Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Dendrites", "Id");
            CreateIndex("dbo.Dendrites", "Neuron_Id");
            CreateIndex("dbo.Neurons", "Layer_Id");
            AddForeignKey("dbo.Layers", "Network_Id", "dbo.Networks", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Layers", "Network_Id", "dbo.Networks");
            DropIndex("dbo.Neurons", new[] { "Layer_Id" });
            DropIndex("dbo.Dendrites", new[] { "Neuron_Id" });
            DropPrimaryKey("dbo.Dendrites");
            AlterColumn("dbo.Neurons", "Layer_Id", c => c.Guid());
            AlterColumn("dbo.Dendrites", "Neuron_Id", c => c.Guid());
            AlterColumn("dbo.Dendrites", "Id", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.Dendrites", "Id");
            CreateIndex("dbo.Neurons", "Layer_Id");
            CreateIndex("dbo.Dendrites", "Neuron_Id");
            AddForeignKey("dbo.Layers", "Network_Id", "dbo.Networks", "Id", cascadeDelete: true);
        }
    }
}
