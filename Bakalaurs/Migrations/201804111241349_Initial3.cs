namespace Bakalaurs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Layers", "Network_Id", "dbo.Networks");
            DropIndex("dbo.Layers", new[] { "Network_Id" });
            AlterColumn("dbo.Layers", "Network_Id", c => c.Guid(nullable: false));
            CreateIndex("dbo.Layers", "Network_Id");
            AddForeignKey("dbo.Layers", "Network_Id", "dbo.Networks", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Layers", "Network_Id", "dbo.Networks");
            DropIndex("dbo.Layers", new[] { "Network_Id" });
            AlterColumn("dbo.Layers", "Network_Id", c => c.Guid());
            CreateIndex("dbo.Layers", "Network_Id");
            AddForeignKey("dbo.Layers", "Network_Id", "dbo.Networks", "Id");
        }
    }
}
