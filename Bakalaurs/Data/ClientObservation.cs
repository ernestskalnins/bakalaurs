﻿using System.IO;
using System.Windows.Media.Imaging;

namespace Bakalaurs.Data
{
    [System.Serializable]
    public class ClientObservation
    {
        public byte[] Image { get; set; }

        public double Left { get; set; }

        public double Forward { get; set; }

        public double Right { get; set; }

        public double PredictLeft { get; set; }

        public double PredictForward { get; set; }

        public double PredictRight { get; set; }

        public BitmapImage BytesToBitmapImage()
        {
            if (Image == null || Image.Length == 0)
            {
                return null;
            }

            var image = new BitmapImage();
            using (var mem = new MemoryStream(Image))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }
    }
}