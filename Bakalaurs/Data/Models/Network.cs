﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalaurs.Data.Models
{
    [Table("Networks")]
    public class Network
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Index("IX_Network__CreatedUtcTimestamp")]
        public DateTime CreatedUtcTimestamp
        {
            get;
            set;
        }

        [InverseProperty("Network")]
        public virtual List<Layer> Layers { get; set; }

        public Network()
        {
            CreatedUtcTimestamp = DateTime.UtcNow;
        }
    }
}
