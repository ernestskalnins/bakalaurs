﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalaurs.Data.Models
{
    [Table("Observations")]
    public class Observation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Index("IX_Observation__UtcTimestamp")]
        public DateTime UtcTimestamp { get; set; }

        [Index("IX_Observation__ControlValueId")]
        public Guid ControlValueId { get; set; }

        [ForeignKey("ControlValueId")]
        public virtual ControlValue ControValue { get; set; }

        [Index("IX_Observation__ImageId")]
        public Guid ImageId { get; set; }

        [ForeignKey("ImageId")]
        public virtual Image Image { get; set; }
    }
}
