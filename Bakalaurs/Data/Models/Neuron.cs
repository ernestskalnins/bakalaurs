﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalaurs.Data.Models
{
    [Table("Neurons")]
    public class Neuron
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Index("IX_Neuron__Index")]
        public int Index
        {
            get;
            set;
        }
        

        public double Bias { get; set; }


        [Index("IX_Neuron__LayerId")]
        public Guid LayerId
        {
            get;
            set;
        }

        public Layer Layer
        {
            get;
            set;
        }

        [InverseProperty("Neuron")]
        public virtual List<Dendrite> Dendrites { get; set; }
    }
}
