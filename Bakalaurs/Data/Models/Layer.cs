﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalaurs.Data.Models
{
    [Table("Layers")]
    public class Layer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Index("IX_Layer__Index")]
        public int Index
        {
            get;
            set;
        }

        [Index("IX_Layer__NetworkId")]
        public Guid NetworkId
        {
            get;
            set;
        }

        [ForeignKey("NetworkId")]
        public Network Network
        {
            get;
            set;
        }

        [InverseProperty("Layer")]
        public virtual List<Neuron> Neurons { get; set; }
    }
}
