﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalaurs.Data.Models
{
    [Table("Dendrites")]
    public class Dendrite
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Index("IX_Dendrite__Index")]
        public int Index
        {
            get;
            set;
        }

        public double Value { get; set; }

        [Index("IX_Dendrite__NeuronId")]
        public Guid NeuronId
        {
            get;
            set;
        }

        [ForeignKey("NeuronId")]
        public Neuron Neuron
        {
            get;
            set;
        }

    }
}
