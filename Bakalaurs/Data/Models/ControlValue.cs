﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalaurs.Data.Models
{
    [Table("ControlValues")]
    public class ControlValue
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public double Left { get; set; }

        public double Forward { get; set; }

        public double Right { get; set; }
    }
}
