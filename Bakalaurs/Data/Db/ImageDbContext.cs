﻿using Bakalaurs.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalaurs.Data.Db
{
    public class ImageDbContext : DbContext
    {

        public DbSet<Image> Images
        {
            get;
            set;
        }

        public DbSet<ControlValue> ControlValues
        {
            get;
            set;
        }

        public DbSet<Observation> Observations
        {
            get;
            set;
        }

        public DbSet<Network> Networks
        {
            get;
            set;
        }

        public DbSet<Layer> Layers
        {
            get;
            set;
        }

        public DbSet<Neuron> Neurons
        {
            get;
            set;
        }

        public DbSet<Dendrite> Dendrites
        {
            get;
            set;
        }
       
        public ImageDbContext() : base("default")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
        }
    }
}
