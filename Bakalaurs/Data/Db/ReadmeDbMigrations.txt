﻿# first time only
Enable-Migrations -ProjectName Bakalaurs -StartUpProjectName Bakalaurs 

# exec when changes in code are made
Add-Migration Initial -ProjectName Bakalaurs -StartUpProjectName Bakalaurs -Force

# update to latest
Update-Database -ProjectName Bakalaurs -StartUpProjectName Bakalaurs -Verbose

# DELETE ALL
Update-Database -TargetMigration:0 -ProjectName Bakalaurs -StartUpProjectName Bakalaurs -Verbose -Force





