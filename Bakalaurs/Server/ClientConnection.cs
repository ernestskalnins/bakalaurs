﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using Bakalaurs.Server.Utilities;
using Bakalaurs.Server.Interfaces;
using Bakalaurs.Data;
using System.Linq;
using System.Drawing;
using System.IO;
using CNNImplementation.CNN;
using System.Collections.Generic;

namespace Bakalaurs.Server
{
    class ClientConnection
    {
        public event Action<ClientObservation> ClientObservationReceived;

        private volatile bool _reading;
        private Socket _connection;
        private Thread _worker;
        private ConnectionPeer _imageReader;
        private ObservationByteConverter _observationByteConverter;
        private CNNWrapper _cnn;
        private int counter = 0;

        public ClientConnection(Socket connection)
        {
            _cnn = new CNNWrapper(0.1);
            _cnn.Load(new Guid("25BB69C8-CE40-E811-B25B-1CB72CABE41A"));
            _connection = connection;
            _observationByteConverter = new ObservationByteConverter();
            ISocketByteTransceiver reader = new SocketByteTransceiver(connection);
            _imageReader = new ConnectionPeer(reader);
        }

        public void StartReading()
        {
            if (!_reading)
            {
                _reading = true;
                _worker = new Thread(this.ReadOrFail);
                _worker.Start();
            }
        }

        public void StartPredicting()
        {
            if (!_reading)
            {
                _reading = true;
                _worker = new Thread(this.PredictOrFail);
                _worker.Start();
            }
        }

        public void Stop()
        {
            _reading = false;
            this.CloseSocket();
            _worker.Join();
        }

        public void CloseSocket()
        {
            try
            {
                _connection.Close();
                _connection.Dispose();
            }
            catch { }
        }

        private void ReadOrFail()
        {
            while (_reading)
            {
                try
                {
                    this.GetObservationData();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Vaivaivaivia, closing client connection: {ex}");
                    this.Stop();
                }
            }
        }

        private void PredictOrFail()
        {
            while (_reading)
            {
                try
                {
                    this.PredictImage();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error, AI became self-aware: {ex}");
                    this.Stop();
                }
            }
        }

        private void GetObservationData()
        {
            byte[] imageBytes = _imageReader.ReadData();
            byte[] leftBytes = _imageReader.ReadData();
            byte[] forwardBytes = _imageReader.ReadData();
            byte[] rightBytes = _imageReader.ReadData();

            ClientObservation observation = new ClientObservation()
            {
                Image = imageBytes,
                Left = BitConverter.ToDouble(leftBytes, 0),
                Forward = BitConverter.ToDouble(forwardBytes, 0),
                Right = BitConverter.ToDouble(rightBytes, 0),
            };
            this.ClientObservationReceived?.Invoke(observation);
        }

        private void PredictImage()
        {
            byte[] imageBytes = _imageReader.ReadData();

            InputImage inputImage = new InputImage(BytesToBitmap(imageBytes));

            double[] predictValues =  _cnn.Network.Predict(inputImage);

            _imageReader.SendData(BitConverter.GetBytes(predictValues[0]));
            _imageReader.SendData(BitConverter.GetBytes(predictValues[1]));
            _imageReader.SendData(BitConverter.GetBytes(predictValues[2]));

            ClientObservation observation = new ClientObservation()
            {
                Image = imageBytes,
                //Steer = steer,
                //Drive = drive,
                PredictLeft = predictValues[0],
                PredictForward = predictValues[1],
                PredictRight = predictValues[2],
            };
            this.ClientObservationReceived?.Invoke(observation);
        }

        private Bitmap BytesToBitmap(byte[] imageData)
        {
            Bitmap bmp;
            using (var ms = new MemoryStream(imageData))
            {
                bmp = new Bitmap(ms);
            }

            return bmp;
        }
    }
}