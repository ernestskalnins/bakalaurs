﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bakalaurs.Server.Interfaces;
using System.Net.Sockets;

namespace Bakalaurs.Server.Utilities
{
    class ConnectionPeer
    {
        private ISocketByteTransceiver _reader;

        public ConnectionPeer(ISocketByteTransceiver reader)
        {
            _reader = reader;
        }

        public byte[] ReadData()
        {
            int payloadSize = this.ReadPayloadSize();
            return _reader.ReadBytes(payloadSize);
        }

        public void SendData(byte[] data)
        {
            this.SendPayloadSize(BitConverter.GetBytes(data.Length));
            _reader.SendBytes(data);
        }

        private int ReadPayloadSize()
        {
            return BitConverter.ToInt32(_reader.ReadBytes(4), 0);
        }

        private void SendPayloadSize(byte[] payloadSize)
        {
            _reader.SendBytes(payloadSize);
        }
    }
}
