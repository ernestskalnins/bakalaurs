﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using Bakalaurs.Server.Interfaces;

namespace Bakalaurs.Server.Utilities
{
    class SocketByteTransceiver : ISocketByteTransceiver
    {
        private Socket _socket;

        public SocketByteTransceiver(Socket socket)
        {
            _socket = socket;
        }

        public byte[] ReadBytes(int bytesToRead)
        {
            int bytesLeft = bytesToRead;
            int bytesRead = 0;
            byte[] result = new byte[bytesToRead];

            Stopwatch stopWatch = Stopwatch.StartNew();

            while (bytesLeft > 0)
            {
                bytesRead += _socket.Receive(result, bytesRead, bytesLeft, SocketFlags.None);
                bytesLeft = bytesToRead - bytesRead;

                if (stopWatch.ElapsedMilliseconds > 5000)
                {
                    throw new Exception("Timeout while reading socket data.");
                }
            }
            return result;
        }

        public void SendBytes(byte[] bytesToSend)
        {
            int bytesLeft = bytesToSend.Length;
            int bytesSent = 0;

            Stopwatch stopWatch = Stopwatch.StartNew();

            while (bytesLeft > 0)
            {
                bytesSent += _socket.Send(bytesToSend, bytesSent, bytesLeft, SocketFlags.None);
                bytesLeft = bytesToSend.Length - bytesSent;

                if (stopWatch.ElapsedMilliseconds > 5000)
                {
                    throw new Exception("Timeout while sending socket data.");
                }
            }
        }
    }
}
