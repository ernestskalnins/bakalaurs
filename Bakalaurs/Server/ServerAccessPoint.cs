﻿using Bakalaurs.Data;
using System;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Threading;

namespace Bakalaurs.Server
{
    class ServerAccessPoint
    {
        public event Action<ClientObservation> ClientObservationReceived;

        private Socket _listener;
        private Thread _listenerThread;
        private IPEndPoint _ipEndPoint;
        private volatile bool _processingClient;

        public ServerAccessPoint(string ipAddress, int port)
        {
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _ipEndPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);
            _listener.Bind(_ipEndPoint);
            _listener.Listen(10);
        }

        public void StartReading()
        {
            if (!_processingClient)
            {
                _processingClient = true;
                _listenerThread = new Thread(this.ProcessDataRead);
                _listenerThread.Start();
            }
        }

        public void StartPredicting()
        {
            if (!_processingClient)
            {
                _processingClient = true;
                _listenerThread = new Thread(this.ProcessPredict);
                _listenerThread.Start();
            }
        }

        private void ProcessDataRead()
        {
            ClientConnection client = null;

            while (_processingClient)
            {
                try
                {
                    Socket clientSocket = _listener.Accept();
                        
                    if (client != null && clientSocket != null)
                    {
                        client.Stop();
                        client = null;
                    }

                    client = new ClientConnection(clientSocket);
                    client.ClientObservationReceived += (observation) =>
                    {
                        this.ClientObservationReceived?.Invoke(observation);
                    };
                    client.StartReading();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error in main accept: {ex}");
                }
            }
            if(client != null)
            {
                client.Stop();
                client = null;
            }
        }

        private void ProcessPredict()
        {
            ClientConnection client = null;

            while (_processingClient)
            {
                try
                {
                    Socket clientSocket = _listener.Accept();
                        
                    if (client != null && clientSocket != null)
                    {
                        client.Stop();
                        client = null;
                    }

                    client = new ClientConnection(clientSocket);
                    client.ClientObservationReceived += (observation) =>
                    {
                        this.ClientObservationReceived?.Invoke(observation);
                    };
                    client.StartPredicting();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error in main accept: {ex}");
                }
            }
            if(client != null)
            {
                client.Stop();
                client = null;
            }
        }

        public void Stop()
        {
            _listener.Close();
            _processingClient = false;
        }
    }
}