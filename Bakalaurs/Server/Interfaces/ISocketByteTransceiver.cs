﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Bakalaurs.Server.Interfaces
{
    interface ISocketByteTransceiver
    {
        byte[] ReadBytes(int byteCount);

        void SendBytes(byte[] data);
    }
}
