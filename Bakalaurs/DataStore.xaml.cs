﻿using Bakalaurs.Data;
using Bakalaurs.Data.Db;
using Bakalaurs.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bakalaurs
{
    /// <summary>
    /// Interaction logic for DataStore.xaml
    /// </summary>
    public partial class DataStore : Page
    {
        private ServerAccessPoint _imageListener;

        public DataStore()
        {
            InitializeComponent();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Server Started");
            Start.IsEnabled = false;
            Stop.IsEnabled = true;
            _imageListener = new ServerAccessPoint("127.0.0.1", 9050);
            _imageListener.ClientObservationReceived += this.ShowImage;
            _imageListener.StartReading();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            if (_imageListener != null)
            {
                Console.WriteLine("Server Stopped");
                Stop.IsEnabled = false;
                Start.IsEnabled = true;
                _imageListener.Stop();
                _imageListener = null;
            }
        }

        private void ShowImage(ClientObservation clientObservation)
        {
            this.SaveObservation(clientObservation);
            Dispatcher.Invoke(() =>
            {
                this.cameraImage.Source = clientObservation.BytesToBitmapImage();
                this.Left.Text = clientObservation.Left.ToString();
                this.Forward.Text = clientObservation.Forward.ToString();
                this.Right.Text = clientObservation.Right.ToString();
            });
        }

        private void SaveObservation(ClientObservation clientObservation)
        {
            using (var db = new ImageDbContext())
            {
                var image = new Data.Models.Image()
                {
                    Data = clientObservation.Image
                };

                var controlValue = new Data.Models.ControlValue()
                {
                    Left = clientObservation.Left,
                    Forward = clientObservation.Forward,
                    Right = clientObservation.Right
                };

                var observation = new Data.Models.Observation()
                {
                    Image = image,
                    UtcTimestamp = DateTime.UtcNow,
                    ControValue = controlValue
                };

                db.Observations.Add(observation);
                db.SaveChanges();
            }
        }
    }
}
