﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN
{
    public class Rectifier
    {
        public InputImage ReLu(InputImage image)
        {
            InputImage rectifiedImage = new InputImage(image.Width, image.Height);
            rectifiedImage.value = image.value;
            for (int i = 0; i < image.Width; i++)
            {
                for (int j = 0; j < image.Height; j++)
                {
                    rectifiedImage[i, j] = Math.Max(0, image[i, j]);
                }
            }
            return rectifiedImage;
        }
    }
}
