﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN.Utils
{
    public class ArrayUtils
    {
        public static void ArrayPrint2D(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write(arr[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public static void ArrayPrint2D(double[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write(arr[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public static double SumArrayValues(double[,] arr)
        {
            double sum = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    sum += arr[i, j];
                }
            }
            return sum;
        }

        public static int SumArrayValues(int[,] arr)
        {
            int sum = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    sum += arr[i, j];
                }
            }
            return sum;
        }

        public static double[,] NormalizeStatistic(int[,] arr)
        {
            double mean = GetMean(arr);
            double standardDeviation = GetStandardDeviation(arr);
            double[,] result = new double[arr.GetLength(0), arr.GetLength(1)];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    result[i, j] = (arr[i, j] - mean) / standardDeviation;
                }
            }
            return result;
        }

        public static double[,] NormalizeMean(int[,] arr)
        {
            double mean = GetMean(arr);
            double[,] result = new double[arr.GetLength(0), arr.GetLength(1)];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    result[i, j] = arr[i, j] - mean;
                }
            }
            return result;
        }

        public static int[,] Scale(double[,] arr, int scale)
        {
            double largestValue = GetLargestValue(arr);
            double smallestValue = GetSmallestValue(arr);
            int[,] result = new int[arr.GetLength(0), arr.GetLength(1)];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    double valMinDelta = arr[i, j] - smallestValue;
                    double minMaxDelta = largestValue - smallestValue;
                    double tmp;
                    if (minMaxDelta != 0)
                    {
                        tmp = valMinDelta / minMaxDelta;
                    }
                    else
                    {
                        tmp = valMinDelta;
                    }

                    int resultVal = (int)(tmp * scale);
                    result[i, j] = resultVal;
                }
            }
            return result;
        }

        public static double[,] Scale(double[,] arr)
        {
            double largestValue = GetLargestValue(arr);
            double smallestValue = GetSmallestValue(arr);
            double[,] result = new double[arr.GetLength(0), arr.GetLength(1)];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    double valMinDelta = arr[i, j] - smallestValue;
                    double minMaxDelta = largestValue - smallestValue;
                    double tmp;
                    if (minMaxDelta != 0)
                    {
                        tmp = valMinDelta / minMaxDelta;
                    }
                    else
                    {
                        tmp = valMinDelta;
                    }

                    double resultVal = tmp;
                    result[i, j] = resultVal;
                }
            }
            return result;
        }

        public static double GetMean(int[,] arr)
        {
            int count = arr.GetLength(0) * arr.GetLength(1);
            double sum = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    sum += arr[i, j];
                }
            }
            return sum / count;
        }

        public static double GetMean(double[,] arr)
        {
            int count = arr.GetLength(0) * arr.GetLength(1);
            double sum = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    sum += arr[i, j];
                }
            }
            return sum / count;
        }

        private static int GetLargestValue(int[,] arr)
        {
            int largest = arr[0, 0];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] > largest)
                        largest = arr[i, j];
                }
            }
            return largest;
        }

        private static double GetLargestValue(double[,] arr)
        {
            double largest = arr[0, 0];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] > largest)
                        largest = arr[i, j];
                }
            }
            return largest;
        }

        private static double GetSmallestValue(int[,] arr)
        {
            double smallest = arr[0, 0];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] < smallest)
                        smallest = arr[i, j];
                }
            }
            return smallest;
        }

        private static double GetSmallestValue(double[,] arr)
        {
            double smallest = arr[0, 0];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] < smallest)
                        smallest = arr[i, j];
                }
            }
            return smallest;
        }

        private static double GetStandardDeviation(int[,] arr)
        {
            double mean = GetMean(arr);
            int count = arr.GetLength(0) * arr.GetLength(1);
            double deltaSum = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    deltaSum += Math.Pow(arr[i, j] - mean, 2);
                }
            }
            return Math.Sqrt(deltaSum / count);
        }
    }
}
