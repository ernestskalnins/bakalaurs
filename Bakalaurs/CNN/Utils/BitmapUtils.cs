﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN.Utils
{
    public class BitmapUtils
    {
        public double[,] CreateGreyscaleBytes(Bitmap bmp)
        {
            double[,] greyscaleBytes = new double[bmp.Width, bmp.Height];
            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    Color c = bmp.GetPixel(i, j);

                    greyscaleBytes[i, j] = (int)(.21 * c.R + .71 * c.G + .071 * c.B);
                }
            }
            return greyscaleBytes;
        }

        public Bitmap CreateGreyscaleImage(int[,] arr)
        {
            Bitmap greyscaleImage = new Bitmap(arr.GetLength(0), arr.GetLength(1));
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    greyscaleImage.SetPixel(i, j, Color.FromArgb(arr[i, j], arr[i, j], arr[i, j]));
                }
            }
            return greyscaleImage;
        }
    }
}
