﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN.Utils
{
    public class StringUtils
    {
        public double[] StringToDoubleArray(string name)
        {
            name = name.Substring(3).Substring(0, 5);
            return Array.ConvertAll(name.Split(','), Double.Parse);
        }

        public List<double> StringToDoubleList(string name)
        {
            double[] values = this.StringToDoubleArray(name);
            return new List<double>(values);
        }
    }
}
