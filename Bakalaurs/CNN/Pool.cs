﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN
{
    public class Pool
    {
        public int Width { get; }
        public int Height { get; }
        public int Stride { get; }

        public Pool(int width, int height, int stride)
        {
            Width = width;
            Height = height;
            Stride = stride; 
        }

        public InputImage MaxPool(InputImage image)
        {
            InputImage pooledImage = this.CreateEmptyImage(image);
            pooledImage.value = image.value;
            for (int i = 0; i < pooledImage.Width; i++)
            {
                for (int j = 0; j < pooledImage.Height; j++)
                {
                    pooledImage[i, j] = GetMax(i * Stride, j * Stride, image);
                }
            }
            return pooledImage;
        }

        public Tuple<int,int> CalculatePooledImageSize(int width, int height)
        {
            return new Tuple<int, int>(width/this.Width, height/this.Height);
        }

        private InputImage CreateEmptyImage(InputImage input)
        {
            Tuple<int, int> imageSize = this.CalculatePooledImageSize(input.Width, input.Height);
            return new InputImage(imageSize.Item1, imageSize.Item2);
        }

        private double GetMax(int x, int y, InputImage image)
        {
            double max = image[x, y];
            for (int i = x; i < x + Width; i++)
            {
                for (int j = y; j < y + Height; j++)
                {
                    if (image[i, j] > max)
                    {
                        max = image[i, j];
                    }
                }
            }
            return max;
        }
    }
}
