﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN.CNNLayers
{
    public class ConvolutionLayer : CNNLayer
    {
        private List<Filter> _filters;
        private Random _rand;

        private ConvolutionLayer()
        {
            _rand = new Random();
        }

        public ConvolutionLayer(List<Filter> filters) : this()
        {
            _filters = filters;
        }

        public ConvolutionLayer(Filter blueprint, int count) : this()
        {
            _filters = new List<Filter>();
            while (count-- != 0)
            {
                _filters.Add(
                    this.CreateRandomFilter(blueprint)    
                );
            }
        }

        public override List<InputImage> Process(List<InputImage> input)
        {
            List<InputImage> processedInput = new List<InputImage>();
            foreach (var image in input)
            {
                foreach (var filter in _filters)
                {
                    processedInput.Add(
                        filter.Apply(image)
                    );
                }
            }
            return processedInput;
        }

        public override Tuple<int,int> GetOutputSize(Tuple<int, int> inputSize)
        {
            return _filters[0].CalculateFeatureSize(inputSize.Item1 * _filters.Count, inputSize.Item2);
        }

        private Filter CreateRandomFilter(Filter blueprint)
        {
            double[,] filterValues = new double[blueprint.Width, blueprint.Height];

            for (int i = 0; i < blueprint.Width; i++)
            {
                for (int j = 0; j < blueprint.Height; j++)
                {
                    filterValues[i, j] = _rand.NextDouble() * 2 - 1;
                }
            }
            return new Filter(filterValues, blueprint.Padding, blueprint.Stride);
        }
    }
}
