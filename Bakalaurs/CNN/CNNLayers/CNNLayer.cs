﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN.CNNLayers
{
    abstract public class CNNLayer
    {
        abstract public List<InputImage> Process(List<InputImage> input);
        abstract public Tuple<int, int> GetOutputSize(Tuple<int, int> inputSize);
    }
}
