﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN.CNNLayers
{
    public class RectifierLayer : CNNLayer
    {
        private Rectifier _rectifier;

        public RectifierLayer(Rectifier rectifier)
        {
            _rectifier = rectifier;
        }

        public override List<InputImage> Process(List<InputImage> input)
        {
            List<InputImage> processedInput = new List<InputImage>();
            foreach (var image in input)
            {
                processedInput.Add(
                    _rectifier.ReLu(image)    
                );
            }
            return processedInput;
        }

        public override Tuple<int,int> GetOutputSize(Tuple<int, int> inputSize)
        {
            return inputSize;
        }
    }
}
