﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN.CNNLayers
{
    public class PoolingLayer : CNNLayer
    {
        private Pool _pool;

        public PoolingLayer(Pool pool)
        {
            _pool = pool;
        }

        public override List<InputImage> Process(List<InputImage> input)
        {
            List<InputImage> processedInput = new List<InputImage>();
            foreach (var image in input)
            {
                processedInput.Add(
                    _pool.MaxPool(image)    
                );
            }
            return processedInput;
        }

        public override Tuple<int, int> GetOutputSize(Tuple<int, int> inputSize)
        {
            return _pool.CalculatePooledImageSize(inputSize.Item1, inputSize.Item2);
        }
    }
}
