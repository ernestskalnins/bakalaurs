﻿using CNNImplementation.CNN;
using CNNImplementation.CNN.CNNLayers;
using CNNImplementation.CNN.NN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN
{
    public class Network
    {
        public NeuralNetwork NeuralNetwork { get; set; }
        public double LearningRate { get; set; }

        private List<CNNLayer> _layers;
        private Random _random;

        public Network(double learningRate) : this()
        {
            LearningRate = learningRate;
        }

        private Network()
        {
            _layers = new List<CNNLayer>();
            _random = new Random();
        }

        public void TrainBatch(List<InputImage> input, int iterations)
        {
            int index;
            for (int i = 0; i < iterations; i++)
            {
                index = _random.Next(0, input.Count);
                List<InputImage> processedImage = this.ProcessImage(input[index]);
                NeuralNetwork.Train(
                    this.FlattenImage(processedImage),
                    input[index].value
                );
            }
        }

        public double[] TrainSingle(InputImage input)
        {
            List<InputImage> processedInput = this.ProcessImage(input);
            return NeuralNetwork.Train(
                this.FlattenImage(processedInput),
                input.value
            );
        }

        public double[] Predict(InputImage input)
        {
            List<InputImage> processedInput = this.ProcessImage(input);
            List<double> flattenedInput = this.FlattenImage(processedInput);

            return NeuralNetwork.Run(flattenedInput);
            //result = this.SoftMax(result);
        }

        public List<InputImage> ProcessImages(List<InputImage> input)
        {
            foreach (var layer in _layers)
            {
                input = layer.Process(input);
            }
            return input;
        }

        public List<InputImage> ProcessImage(InputImage input)
        {
            List<InputImage> inputWrapper = new List<InputImage>();
            inputWrapper.Add(input);
            foreach (var layer in _layers)
            {
                inputWrapper = layer.Process(inputWrapper);
            }
            return inputWrapper;
        }

        public Network AddConvolutionLayer(List<Filter> filters)
        {
            _layers.Add(
                new ConvolutionLayer(filters)
            );
            return this;
        }

        public Network AddConvolutionLayer(Filter filter, int count)
        {
            _layers.Add(
                new ConvolutionLayer(filter, count)
            );
            return this;
        }

        public Network AddRectifierLayer(Rectifier rectifier)
        {
            _layers.Add(
                new RectifierLayer(rectifier)
            );
            return this;
        }

        public Network AddPoolingLayer(Pool pool)
        {
            _layers.Add(
                new PoolingLayer(pool)    
            );
            return this;
        }

        public void AddFullyConnectedLayer(int imageWidth, int imageHeight, int categoryCount)
        {
            int nnInputSize = this.CalculateOutputImageSize(imageWidth, imageHeight);
            int[] nnLayers = new int[3]
            {
                nnInputSize,
                (nnInputSize + categoryCount) / 2,
                categoryCount
            };
            NeuralNetwork = new NeuralNetwork(LearningRate, nnLayers);
        }

        public int CalculateOutputImageSize(int originalWidth, int originalHeight)
        {
            Tuple<int, int> inputSize = new Tuple<int, int>(originalWidth, originalHeight);
            foreach (var layer in _layers)
            {
                inputSize = layer.GetOutputSize(inputSize);
            }
            return inputSize.Item1 * inputSize.Item2;
        }

        private List<double> FlattenImage(List<InputImage> features)
        {
            List<double> flattenedImages = new List<double>();
            foreach (var feature in features)
            {
                flattenedImages.AddRange(feature.ToDoubleList());
            }
            return flattenedImages;
        }

        private double[] SoftMax(double[] data)
        {
            double[] exponents = new double[data.Length];
            double[] percentages = new double[data.Length];
            double sum = 0;
            for (int i = 0; i < data.Length; i++)
            {
                exponents[i] = Math.Exp(data[i]);
                sum += exponents[i];
            }
            for (int i = 0; i < exponents.Length; i++)
            {
                percentages[i] = exponents[i] / sum;
            }
            return percentages;
        }
    }
}
