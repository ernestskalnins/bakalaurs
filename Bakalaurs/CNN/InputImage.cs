﻿using CNNImplementation.CNN.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN
{
    public class InputImage
    {
        public int Width
        {
            get { return _imageBytes.GetLength(0); }
        }

        public int Height
        {
            get { return _imageBytes.GetLength(1); }
        }

        public double this[int x, int y]
        {
            get
            {
                return _imageBytes[x, y];
            }
            set
            {
                _imageBytes[x, y] = value;
            }
        }
        public List<double> value;

        private double[,] _imageBytes;
        private BitmapUtils _utils;

        private InputImage()
        {
            _utils = new BitmapUtils();
        }

        public InputImage(Bitmap image) : this()
        {
            _imageBytes = _utils.CreateGreyscaleBytes(image);
        }

        public InputImage(int width, int height) : this()
        {
            _imageBytes = new double[width, height];
        }

        public Bitmap ToBitmap()
        {
            return _utils.CreateGreyscaleImage(ArrayUtils.Scale(_imageBytes, 255));
        }

        public List<double> ToDoubleList()
        {
            List<double> values = new List<double>();
            double[,] scaledImage = ArrayUtils.Scale(_imageBytes);
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    values.Add(scaledImage[i, j]);
                }
            }
            return values;
        }

        public void Print2D()
        {
            ArrayUtils.ArrayPrint2D(_imageBytes);
        }

        public InputImage Pad(int padding)
        {
            if (padding <= 0)
            {
                return this;
            }
            int paddedWidth = Width + 2 * padding;
            int paddedHeight = Height + 2 * padding;
            InputImage paddedImage = new InputImage(paddedWidth, paddedHeight);
            paddedImage.value = this.value;
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    paddedImage[i + padding, j + padding] = this[i, j];
                }
            }
            return paddedImage;
        }
    }
}
