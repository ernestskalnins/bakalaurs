﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN
{
    public class Filter
    {
        public int Width
        {
            get { return _filter.GetLength(0); }
        }

        public int Height
        {
            get { return _filter.GetLength(1); }
        }

        public int Padding { get; }
        public int Stride { get; }

        private double[,] _filter;

        public Filter(double[,] filter, int padding, int stride)
        {
            _filter = filter;
            Padding = padding;
            Stride = stride;
        }

        public InputImage Apply(InputImage input)
        {
            InputImage feature = this.CreateEmptyFeature(input);
            feature.value = input.value;
            input = input.Pad(Padding);
            for (int i = 0; i < feature.Width; i++)
            {
                for (int j = 0; j < feature.Height; j++)
                {
                    feature[i, j] = this.GetAverageSegmentValue(i * Stride, j * Stride, input);
                }
            }
            return feature;
        }

        public Tuple<int, int> CalculateFeatureSize(int width, int height)
        {
            int featureWidth = (width + 2 * Padding - this.Width) / Stride + 1;
            int featureHeight = (height + 2 * Padding - this.Height) / Stride + 1;
            return new Tuple<int, int>(featureWidth, featureHeight);
        }

        private InputImage CreateEmptyFeature(InputImage input)
        {
            Tuple<int, int> featureSize = this.CalculateFeatureSize(input.Width, input.Height);
            return new InputImage(featureSize.Item1, featureSize.Item2);
        }

        private double GetAverageSegmentValue(int x, int y, InputImage input)
        {
            double segmentSum = 0;
            int filterWidthEnd = (int)Math.Ceiling(Width / 2.0f);
            int filterHeightEnd = (int)Math.Ceiling(Height / 2.0f);
            int fi = 0, fj = 0;
            for (int i = x; i <= x + filterWidthEnd; i++)
            {
                for (int j = y; j <= y + filterHeightEnd; j++)
                {
                    segmentSum += input[i, j] * _filter[fi, fj];
                    fi++;
                }
                fi = 0;
                fj++;
            }
            return segmentSum;
        }
    }
}
