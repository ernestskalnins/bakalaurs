﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNNImplementation.CNN.NN
{
    public class Neuron
    {
        public List<Dendrite> Dendrites { get; set; }
        public double Bias { get; set; }
        public double Delta { get; set; }
        public double Value { get; set; }

        public int DendriteCount
        {
            get { return Dendrites.Count; }
        }

        public Neuron()
        {
            Random r = new Random(Environment.TickCount);
            this.Bias = r.NextDouble();
            this.Dendrites = new List<Dendrite>();
        }
    }
}
