﻿using Bakalaurs.Data.Db;
using Bakalaurs.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bakalaurs
{
    /// <summary>
    /// Interaction logic for NNTrainer.xaml
    /// </summary>
    public partial class NNTrainer : Page
    {
        private CNNWrapper _cnn;
        private NetworkTrainer _trainer;

        public NNTrainer()
        {
            InitializeComponent();
            _cnn = new CNNWrapper(1);
            _trainer = new NetworkTrainer();
            GetNetworkGuidList();
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            _cnn.Store();
            GetNetworkGuidList();
            NetworkList.Items.Refresh();
            Status.Text = "Created new network.";
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            if (NetworkList.SelectedValue != null)
            {
                Guid networkId = (Guid)NetworkList.SelectedValue;
                _cnn.Load(networkId);
                Status.Text = "Loaded network " + networkId.ToString();
            }
        }

        private void Train_Click(object sender, RoutedEventArgs e)
        {
            Status.Text = "Training...";
            int iterations = Convert.ToInt32(IterationCount.Text);
            double learningRate = Convert.ToDouble(LearningRate.Text);
            _cnn.Network.LearningRate = learningRate;
            _cnn.Network = _trainer.Train(_cnn.Network, iterations);
            Status.Text = "Training complete!";
        }

        private void GetNetworkGuidList()
        {
            using (var db = new ImageDbContext())
            {
                NetworkList.ItemsSource = db.Networks.Select(n => n.Id).ToList();
            }
        }
    }
}
