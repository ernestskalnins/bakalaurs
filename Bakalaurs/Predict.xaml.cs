﻿using Bakalaurs.Data;
using Bakalaurs.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bakalaurs
{
    /// <summary>
    /// Interaction logic for Predict.xaml
    /// </summary>
    public partial class Predict : Page
    {
        private ServerAccessPoint _imageListener;

        public Predict()
        {
            InitializeComponent();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Server Started");
            Start.IsEnabled = false;
            Stop.IsEnabled = true;
            _imageListener = new ServerAccessPoint("127.0.0.1", 9050);
            _imageListener.ClientObservationReceived += this.ShowImage;
            _imageListener.StartPredicting();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            if (_imageListener != null)
            {
                Console.WriteLine("Server Stopped");
                Stop.IsEnabled = false;
                Start.IsEnabled = true;
                _imageListener.Stop();
                _imageListener = null;
            }
        }

        private void ShowImage(ClientObservation clientObservation)
        {
            Dispatcher.Invoke(() =>
            {
                this.cameraImage.Source = clientObservation.BytesToBitmapImage();
                this.PredictLeft.Text = clientObservation.PredictLeft.ToString();
                this.PredictForward.Text = clientObservation.PredictForward.ToString();
                this.PredictRight.Text = clientObservation.PredictRight.ToString();
            });
        }

    }
}
